#ifndef QUITMENUITEM_H
#define QUITMENUITEM_H

#include "MenuItem.h"

class QuitMenuItem : public MenuItem
{
    public:
        QuitMenuItem(string name): MenuItem(name) {}
        virtual bool do_stuff();
};

#endif // QUITMENUITEM_H
