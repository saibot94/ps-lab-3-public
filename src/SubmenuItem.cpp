#include "SubmenuItem.h"

/* TODO

    Use this function in order to validate if a user selection is correct or not.
    To read a user selection, write something like: cin >> option.
*/
bool SubmenuItem::valid_option(int i)
{
    if(i < 0 || (unsigned)i > menu_items.size()) {
        return false;
    }
    return true;
}

/* TODO

Implement this
*/
bool SubmenuItem::do_stuff()
{
    int option;
    cout << "- " << get_name() << endl;
    cout << "==================" << endl;
    cout << "Choose your option: ";
    cin >> option;
    return false;
 }
